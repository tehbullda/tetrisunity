﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreFeedback : MonoBehaviour
{
    public float lifetime;
    private float currentTime = 0.0f;
    private Text text;
    private Vector3 startPos;
    private Vector3 goalPos;
    // Use this for initialization
    void Start()
    {
        startPos = transform.position;
        goalPos = transform.parent.parent.GetChild(0).transform.position;
    }
    public void SetScoreText(int score)
    {
        text = GetComponent<Text>();
        text.text = "+" + score.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > lifetime)
        {
            Destroy(gameObject);
        }
        else
            ManualLerp();
    }
    private void ManualLerp()
    {
        float percentage = lifetime / currentTime;
        Vector3 direction = goalPos - startPos;
        transform.position = startPos + direction * percentage;
    }
}
