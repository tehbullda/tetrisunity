﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    private Text text;
    private static int currentScore;
    public static int singleClearScore = 250, doubleClearScore = 600, tripleClearScore = 1000, tetrisClearScore = 2000;
    public ScoreFeedbackSpawner spawner;
	// Use this for initialization
	void Start () {
        currentScore = 0;
        text = GetComponent<Text>();
        text.text = "Score: " + currentScore;
	}
	public void RowsCleared(int numRows)
    {
        switch(numRows)
        {
            case 1: UpdateScoreAdd(singleClearScore);
                break;
            case 2: UpdateScoreAdd(doubleClearScore);
                break;
            case 3: UpdateScoreAdd(tripleClearScore);
                break;
            case 4: UpdateScoreAdd(tetrisClearScore);
                break;
        }
    }
	public void UpdateScoreSet(int newvalue)
    {
        currentScore = newvalue;
        UpdateText();
    }
    private void UpdateScoreAdd(int change)
    {
        currentScore += change;
        spawner.Spawn(change);
        UpdateText();
    }
    void UpdateText()
    {
        text.text = "Score: " + currentScore;
    }
}
