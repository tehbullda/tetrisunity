﻿using UnityEngine;
using System.Collections;

public class ScoreFeedbackSpawner : MonoBehaviour {

    public GameObject feedbackObj;
    public Transform scorePos;
    void Start()
    {
        Spawn(5);
    }
	public void Spawn(int score)
    {
        GameObject tmp = Instantiate(feedbackObj, scorePos.position + new Vector3(-500,300,-3), Quaternion.identity) as GameObject;
        tmp.transform.SetParent(transform, false);
        tmp.GetComponent<ScoreFeedback>().SetScoreText(score);
    }

}
