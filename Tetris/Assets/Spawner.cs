﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

    public GameObject[] groups;
    private GameObject queued = null;
    public GameObject queuedPos;
    // Use this for initialization
    void Start()
    {
        spawnNext();
        AddToQueue();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void AddToQueue()
    {
        int i = Random.Range(0, groups.Length);
        //queued = new GameObject();
        queued = Instantiate(groups[i],
                    queuedPos.transform.position,
                    Quaternion.identity) as GameObject;
    }
    public void spawnNext()
    {
        GameObject tmp;
        if (queued != null)
        {
            tmp = queued;
            tmp.transform.position = transform.position;
            tmp.GetComponent<Group>().Activate();
            AddToQueue();
        }
        else
        {
            int i = Random.Range(0, groups.Length);
            tmp = Instantiate(groups[i],
                        transform.position,
                        Quaternion.identity) as GameObject;
            tmp.GetComponent<Group>().Activate();
        }
    }
}
