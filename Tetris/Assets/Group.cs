﻿using UnityEngine;
using System.Collections;

public class Group : MonoBehaviour
{
    private enum Dir
    {
        Left,
        Right,
        Down,
        None
    }
    float lastFall = 0, holdCooldown = 0.15f, currentHoldCooldown = 0.0f, dropSpawnCD = 0.15f;
    private Dir currentHeldIndex = Dir.None;
    private GameObject ghost;
    // Use this for initialization
    void Start()
    {
        // Default position not valid? Then it's game over
        if (!isValidGridPos())
        {
            Debug.Log("GAME OVER");
            Destroy(gameObject);
        }
        SetupGhost();
    }

    // Update is called once per frame
    void Update()
    {
        currentHoldCooldown -= Time.deltaTime;
        dropSpawnCD = dropSpawnCD > 0.0f ? dropSpawnCD - Time.deltaTime : dropSpawnCD;
        // Move Left
        if (Input.GetKey(KeyCode.LeftArrow) && (currentHeldIndex != Dir.Left || currentHoldCooldown < 0.0f))
        {
            currentHeldIndex = Dir.Left;
            currentHoldCooldown = holdCooldown;
            // Modify position
            transform.position += new Vector3(-1, 0, 0);

            // See if valid
            if (isValidGridPos())
                // Its valid. Update grid.
                updateGrid();
            else
                // Its not valid. revert.
                transform.position += new Vector3(1, 0, 0);
            ResetGhostPos();
        }
        // Move Right
        if (Input.GetKey(KeyCode.RightArrow) && (currentHeldIndex != Dir.Right || currentHoldCooldown < 0.0f))
        {
            currentHeldIndex = Dir.Right;
            currentHoldCooldown = holdCooldown;
            // Modify position
            transform.position += new Vector3(1, 0, 0);

            // See if valid
            if (isValidGridPos())
                // It's valid. Update grid.
                updateGrid();
            else
                // It's not valid. revert.
                transform.position += new Vector3(-1, 0, 0);
            ResetGhostPos();
        }
        // Rotate
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Rotate(0, 0, -90);
            ghost.transform.Rotate(0, 0, -90);
            // See if valid
            if (isValidGridPos())
                // It's valid. Update grid.
                updateGrid();
            else
            {
                // Make it work better at borders
                bool successful = false;
                transform.position += new Vector3(-1, 0, 0);
                if (isValidGridPos())
                {
                    // It's valid. Update grid.
                    successful = true;
                    updateGrid();
                }
                else
                {
                    transform.position -= new Vector3(1, 0, 0);
                }
                if (!successful)
                {
                    transform.position += new Vector3(1, 0, 0);
                    if (isValidGridPos())
                    {
                        // It's valid. Update grid.
                        successful = true;
                        updateGrid();
                    }
                    else
                        transform.position -= new Vector3(-1, 0, 0);
                }
                if (!successful)
                {
                    // It's not valid. revert.
                    transform.Rotate(0, 0, 90);
                    ghost.transform.Rotate(0, 0, 90);
                }
                else
                {
                    ResetGhostPos();
                }
            }
            ResetGhostPos();
        }
        // Move Downwards and Fall
        if ((Input.GetKey(KeyCode.DownArrow) && currentHoldCooldown < 0.0f) ||
                 Time.time - lastFall >= 1)
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                currentHeldIndex = Dir.Down;
                currentHoldCooldown = holdCooldown;
            }
            // Modify position
            transform.position += new Vector3(0, -1, 0);

            // See if valid
            if (isValidGridPos())
            {
                // It's valid. Update grid.
                updateGrid();
            }
            else
            {
                // It's not valid. revert.
                transform.position += new Vector3(0, 1, 0);

                // Clear filled horizontal lines
                Grid.deleteFullRows();

                // Spawn next Group
                FindObjectOfType<Spawner>().spawnNext();

                // Disable script
                enabled = false;
                Destroy(ghost);
                GetComponent<ParticleSystem>().Play();
            }

            lastFall = Time.time;
        }
        // Drop
        if (Input.GetKeyDown(KeyCode.Space) && dropSpawnCD < 0.0f)
        {
            while (true)
            {
                transform.position += new Vector3(0, -1, 0);
                if (isValidGridPos())
                    updateGrid();
                else
                    break;
            }
            // It's not valid. revert.
            transform.position += new Vector3(0, 1, 0);

            // Clear filled horizontal lines
            Grid.deleteFullRows();

            // Spawn next Group
            FindObjectOfType<Spawner>().spawnNext();

            // Disable script
            enabled = false;
            Destroy(ghost);
            GetComponent<ParticleSystem>().Play();
        }
        DrawGhostPiece();
    }
    private void ResetGhostPos()
    {
        ghost.transform.position = transform.position;
    }
    private void SetupGhost()
    {
        ghost = Instantiate(gameObject, transform.position, Quaternion.identity) as GameObject;
        ghost.name = transform.name + "'s ghost";
        Destroy(ghost.GetComponent<Group>());
        for (int i = 0; i < ghost.transform.childCount; ++i)
        {
            Color ghostcolor = ghost.transform.GetChild(i).GetComponent<SpriteRenderer>().color;
            ghostcolor.a = 0.25f;
            ghost.transform.GetChild(i).GetComponent<SpriteRenderer>().color = ghostcolor;
        }
    }
    private void DrawGhostPiece()
    {
        while (true)
        {
            ghost.transform.position += new Vector3(0, -1, 0);
            if (!isValidGhostPos())
                break;
        }
        // It's not valid. revert.
        ghost.transform.position += new Vector3(0, 1, 0);
    }
    public void Activate()
    {
        enabled = true;
    }
    bool isValidGhostPos()
    {
        for (int i = 0; i < ghost.transform.childCount; ++i)
        {
            Vector2 v = Grid.roundVec2(ghost.transform.GetChild(i).transform.position);

            // Not inside Border?
            if (!Grid.insideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (Grid.grid[(int)v.x, (int)v.y] != null && Grid.grid[(int)v.x, (int)v.y].parent != transform && Grid.grid[(int)v.x, (int)v.y].parent != ghost.transform)
                return false;
        }
        return true;
    }
    bool isValidGridPos()
    {
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.roundVec2(child.position);

            // Not inside Border?
            if (!Grid.insideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (Grid.grid[(int)v.x, (int)v.y] != null &&
                Grid.grid[(int)v.x, (int)v.y].parent != transform)
                return false;
        }
        return true;
    }
    void updateGrid()
    {
        // Remove old children from grid
        for (int y = 0; y < Grid.h; ++y)
            for (int x = 0; x < Grid.w; ++x)
                if (Grid.grid[x, y] != null)
                    if (Grid.grid[x, y].parent == transform)
                        Grid.grid[x, y] = null;

        // Add new children to grid
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.roundVec2(child.position);
            Grid.grid[(int)v.x, (int)v.y] = child;
        }
    }
}
